pip3 install setuptools
pip3 install -Iv beautifulsoup4==4.6.1
pip3 install -Iv bleach==2.1.3
pip3 install -Iv certifi==2018.4.16
pip3 install -Iv chardet==3.0.4
pip3 install -Iv feedparser==5.2.1
pip3 install -Iv html5lib==1.0.1
pip3 install -Iv idna==2.7
pip3 install -Iv praw==6.0.0
pip3 install -Iv prawcore==1.0.0
pip3 install -Iv requests==2.19.1
pip3 install -Iv six==1.11.0
pip3 install -Iv update-checker==0.16
pip3 install -Iv urllib3==1.23
pip3 install -Iv webencodings==0.5.1