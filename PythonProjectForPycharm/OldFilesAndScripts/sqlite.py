import sqlite3

sqlite_file = 'feed_sources_and_urls_database.sqlite'
table_name = 'feed_database'
feed_sources = 'feed_sources'
feed_urls = 'feed_urls'
field_type = 'varchar'

def connect(sqlite_file):
    """ Make connection to an SQLite database file """
    conn = sqlite3.connect(sqlite_file)
    c = conn.cursor()
    return conn, c

def close(conn):
    """ Commit changes and close connection to the database """
    conn.commit()
    conn.close()

def CreateNewDatabase(sqlite_file, table_name, feed_sources, feed_urls, field_type):
    conn, c = connect(sqlite_file)

    try:
        c.execute('CREATE TABLE {tn} ({nf} {ft} PRIMARY KEY)'.format(tn=table_name, nf=feed_sources, ft=field_type))
    except Exception as e:
        s = str(e)
        print("Query ignored -> You tried to create a table that already exists: " + s)
        pass

    try:
        c.execute("ALTER TABLE {tn} ADD COLUMN '{cn}' {ct}".format(tn=table_name, cn=feed_urls, ct=field_type))
    except Exception as e:
        s = str(e)
        print("Query ignored -> Duplicate column name: " + s)
        pass

    close(conn)


def PopulateDatabase(sqlite_file, table_name, feed_sources, feed_urls, source_value,feeds_value):
    conn, c = connect(sqlite_file)

    try:
        c.execute("INSERT INTO {tn} ({fs}, {fu}) VALUES (?, ?)"
                  .format(tn=table_name, fs=feed_sources, fu=feed_urls), (source_value, feeds_value))

    except sqlite3.IntegrityError:
        print('ERROR: ID already exists in PRIMARY KEY column {}'.format(source_value))

    close(conn)

def ShowDataFromAFieldRow(sqlite_file, table_name, feed_sources, feed_urls, row_value):
    conn, c = connect(sqlite_file)

    # 1) Contents of all columns for row that match a certain value in 1 column
    c.execute("SELECT * FROM {tn} WHERE {fs}={rv}".format(tn=table_name, fu=feed_urls, fs=feed_sources, rv=row_value))
    all_rows = c.fetchall()
    # print('1):', all_rows)

    conn.close()
    return all_rows

def ShowAllData(sqlite_file, table_name):
    conn = sqlite3.connect(sqlite_file)
    c = conn.cursor()

    c.execute("SELECT * FROM {tn}".format(tn=table_name))
    all_rows = c.fetchall()

    conn.close()
    return all_rows

def ExecuteCustomSQLQuery(sqlite_file, sql_query, boolean_return):
    sqlite_file = 'feed_sources_and_urls_database.sqlite'
    table_name = 'feed_database'
    feed_sources = 'feed_sources'
    feed_urls = 'feed_urls'
    field_type = 'varchar'
    conn, c = connect(sqlite_file)
    print(sql_query)

    try:
        c.executescript(sql_query)
    except sqlite3.IntegrityError:
        print('ERROR: the above INSERT OPERATION FAILED.')


    if (boolean_return is "No"):
        conn.close()
    if (boolean_return is "Yes"):
        all_rows = c.fetchall()
        conn.close()
        return all_rows

def ExecuteCustomSQLQueryWithArguments2(attributes, params):
    conn, c = connect(attributes[0][0])

    try:
        c.execute("INSERT INTO {table_name} ({feed_sources}, {feed_urls}) VALUES (?, ?)"
                  .format(table_name=attributes[0][1], feed_sources=attributes[1][0], feed_urls=attributes[1][1]), params)

    except sqlite3.IntegrityError:
        print('ERROR: the above INSERT OPERATION FAILED.')

    close(conn)

def ExecuteCustomSQLQueryWithArguments(sqlite_file, sql_query, arguments, boolean_return):
    conn, c = connect(sqlite_file)


    sql_query = sql_query.format(table_name=arguments[0], feed_sources=arguments[1], var_one=arguments[2])
    print(sql_query)

    try:
        c.execute(sql_query)

    except sqlite3.IntegrityError:
        print('ERROR: the above INSERT OPERATION FAILED.')


    if (boolean_return is "No"):
        conn.close()
    if (boolean_return is "Yes"):
        all_rows = c.fetchall()
        conn.close()
        return all_rows

# var_one = "https://www.kaldata.com/feed"
# print(ExecuteCustomSQLQueryWithArguments(sqlite_file, """   SELECT
#                                                                                         *
#                                                                                     FROM
#                                                                                         {table_name}
#                                                                                     WHERE
#                                                                                         {feed_sources} = {var_one};""",
#                                              [table_name, feed_sources, var_one], "Yes"))

sqlite_file = 'feed_sources_and_urls_database.sqlite'
table_name = 'feed_database'
feed_sources = 'feed_sources'
feed_urls = 'feed_urls'
field_type = 'varchar'

var1='http://boards.4chan.org/g/thread/67215900#11111111111'
#http://boards.4chan.org/g/thread/67217039#67217039
var2='http://boards.4chan.org/g/thread/67215900#67215900'
ExecuteCustomSQLQuery(sqlite_file, """ UPDATE feed_database SET feed_urls = '{var1}' WHERE feed_urls = '{var2}'; """.format(var1=var1, var2=var2), "Yes")

# PopulateDatabase(sqlite_file, table_name, feed_sources, feed_urls, '2222222', '111111111')
# print(ShowAllData(sqlite_file, table_name))
#
# print(ExecuteCustomSQLQuery(sqlite_file, """SELECT
#                                                                     *
#                                                                 FROM
#                                                                     {table_name}""".format(table_name=table_name),
#                             "Yes"))
#
# print(ExecuteCustomSQLQuery(sqlite_file, """UPDATE
#                                                                     {table_name}
#                                                                 SET
#                                                                     {feed_urls} = '{var_one}'
#                                                                 WHERE
#                                                                     {feed_sources} = {var_two}"""
#                             .format(table_name=table_name, feed_sources=feed_sources,
#                                     feed_urls=feed_urls, var_one='asadsadasda',
#                                     var_two='222222222'), "No"))
#
# var_one = '2222222'
# print(ExecuteCustomSQLQueryWithArguments(sqlite_file, """   SELECT
#                                                                                     *
#                                                                                 FROM
#                                                                                     {table_name}
#                                                                                 WHERE
#                                                                                     {var_one} = {feed_sources}""",
#                                          [table_name, feed_sources, var_one], "Yes"))

# params = ('999999', 'oooooo')
# system_params = (sqlite_file, table_name)
# variables_params = (feed_sources, feed_urls)

